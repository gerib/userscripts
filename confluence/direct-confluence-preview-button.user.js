// ==UserScript==
// @name         Direct Confluence Preview Button
// @description  Adds a directly accessible preview button to Confluence pages in edit mode
// @version      2021.11.19-1321
// @author       Gerold 'Geri' Broser <https://stackoverflow.com/users/1744774>
// @icon         https://upload.wikimedia.org/wikipedia/commons/thumb/a/af/Logo_Confluence.svg/1280px-Logo_Confluence.svg.png
// @license      GNU GPLv3 <http://www.gnu.org/licenses/gpl-3.0.html>
// @homepage     https://gitlab.com/gerib/userscripts/-/wikis/home
// @supportURL   https://gitlab.com/gerib/userscripts/-/issues
// @downloadURL  https://gitlab.com/gerib/userscripts/-/raw/master/confluence/direct-confluence-preview-button.user.js
// @updateURL    https://gitlab.com/gerib/userscripts/-/raw/master/confluence/direct-confluence-preview-button.user.js
// ----------------------------------------------------------------------------
// @namespace    igb
// @include      https://confluence.*/pages/*
// @run-at       document-idle
// @grant        none
// ==/UserScript==

(function() {
    'use strict';

    try {
        console.log("BEGIN Direct Confluence Preview Button...");

        const DEBUG = true // set to >true< for debugging

        let save = document.getElementById('rte-button-publish').parentNode
        if ( DEBUG ) console.debug( "save:", save )

        let preview = document.getElementById('rte-button-preview').cloneNode(true)
        preview.classList.add('aui-button')
        if ( DEBUG ) console.debug( "preview:", preview )

        const div = document.createElement('div')
        div.appendChild(preview)
        div.classList.add('aui-buttons')

        save.parentNode.insertBefore(div, save)

        console.log("END Direct Confluence Preview Button.")
    }
    catch (e) {
        console.error(e)
    }

})();
