// ==UserScript==
// @name        StackExchange - Tweak UI
// @description Tweaks StackExchange's UI by removing noise etc.
// @version     21.11.20-2150
// @author      Gerold 'Geri' Broser <https://stackoverflow.com/users/1744774>
// @icon        https://cdn.sstatic.net/stackexchange/img/logos/se/se-icon.png
// @license     GNU GPLv3 <http://www.gnu.org/licenses/gpl-3.0.html>
// @homepage    https://gitlab.com/gerib/userscripts/-/wikis/
// @supportURL  https://gitlab.com/gerib/userscripts/-/issues
// @downloadURL https://gitlab.com/gerib/userscripts/-/raw/master/stackexchange/StackExchange%20-%20Tweak%20UI.user.js
// @updateURL   https://gitlab.com/gerib/userscripts/-/raw/master/stackexchange/StackExchange%20-%20Tweak%20UI.user.js
// --------------------------------------------------
// @namespace   igb
// @include /^https:\/\/(\w*\.+)?(stack|ask|code|server|super|math)\w*\.(com|net)\/questions\/?.*$/
// Matches:             |<none> ||      ⬑– Domains that don't follow |  | TLDs  || Paths        |
//                      | meta. ||      the <something>.stackexchange|           | + possible   |
//                      |       ||      scheme...                    |           |   fragment   |
//                      |⬑–––––––⬑–––– ...and those that do.        |
//              ---- extend bracket expression(s) above for new site(s) ----

// @exclude /^https:\/\/(\w*\.+)?(stack|ask|code|server|super|math)\w*\.(com|net)\/questions\/ask\/?.*$/
// Matches:             |<none> ||      ⬑– Domains that don't follow |  | TLDs  || Path              |
//                      | meta. ||      the <something>.stackexchange|           |+ possible fragment|
//                      |       ||      scheme...                    |
//                      |⬑–––––––⬑–––– ...and those that do.        |
//              ---- extend bracket expression(s) above for new site(s) ----
// @run-at      document-idle
// @grant       none
// ==/UserScript==

(function() {
	'use strict'

	try {
		console.log("BEGIN StackExchange - Tweak UI...");

		const DEBUG = false // set to »true« for debugging

		setTimeout( () => {
			console.log('BEGIN StackExchange - Tweak UI...delayed...')

			document.getElementById('left-sidebar').remove()

			const container = document.querySelector('.container')
			container.style.maxWidth = 'none'

			const content = document.getElementById('content')
			if ( DEBUG ) console.debug('>> content:', content )
			content.style.maxWidth = 'none'

			console.log("END StackExchange - Tweak UI...delayed.")
		}, 1000 )

		console.log("END StackExchange - Tweak UI.")
	}
	catch (e) {
		console.error(e)
	}

})();
