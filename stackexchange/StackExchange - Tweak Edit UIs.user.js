// ==UserScript==
// @name        StackExchange - Tweak Edit UIs
// @description Tweaks StackExchange's Ask and Edit UIs by removing noise, positioning preview side-by-side, enlarging edit area.
// @version     24.01.24-0027
// @author      Gerold 'Geri' Broser <https://stackoverflow.com/users/1744774>
// @icon        https://cdn.sstatic.net/stackexchange/img/logos/se/se-icon.png
// @license     GNU GPLv3 <http://www.gnu.org/licenses/gpl-3.0.html>
// @homepage    https://gitlab.com/gerib/userscripts/-/wikis/
// @supportURL  https://gitlab.com/gerib/userscripts/-/issues
// @downloadURL https://gitlab.com/gerib/userscripts/-/raw/master/stackexchange/StackExchange%20-%20Tweak%20Edit%20UIs.user.js
// @updateURL   https://gitlab.com/gerib/userscripts/-/raw/master/stackexchange/StackExchange%20-%20Tweak%20Edit%20UIs.user.js
// --------------------------------------------------
// @namespace   igb

// @include /^https:\/\/(\w*\.+)?(stack|ask|server|super|math)\w+\.(com|net)\/.+\/(ask|edit)$/
// Matches:             |<none> |       ⬑– Domains that don't      | TLDs  | Paths         |
//                      | meta. |          follow the <something>. |       |               |
//                      |       |          stackexchange  scheme   |       |               |
//                      |⬑–––––––⬑–––– ...and those that do.        |
//              ---- extend bracket expression(s) above for new site(s) ----

// @run-at      document-idle
// @grant       none
// ==/UserScript==

/**
* TODO
*   1. Test with other browsers than Firefox v121.0.1.
*   2. Test with other userscript add-ons than Tampermonkey v5.01.
*/
(function() {
	'use strict'

	try {
		console.log("BEGIN StackExchange - Tweak UI...");

		const DEBUG = false // set to »true« for debugging

		setTimeout( () => {
			console.log('BEGIN StackExchange - Tweak UI...delayed...')

			// See 'How to get the browser viewport dimensions?' at https://stackoverflow.com/a/8876069/1744774
			const VIEWPORT_HEIGHT = Math.max(document.documentElement.clientHeight, window.innerHeight || 0) - 192

			const container = document.querySelector('.container')
			if ( DEBUG ) console.debug('>> container:', container )
			container.style.maxWidth = 'none'

			const content = document.getElementById('content')
			if ( DEBUG ) console.debug('>> content:', content )
			content.style.maxWidth = 'none'

			//const askRightSidebar = document.getElementsByName('aside') // .remove() // Why empty NodeList?!?
			const askRightSidebar = document.querySelector('.fl-shrink0')
			if ( DEBUG ) console.debug('>> askRightSidebar:', askRightSidebar )
			if ( askRightSidebar ) askRightSidebar.remove() // on Ask page only

			const editRightSidebar = document.getElementById('sidebar')
			if ( DEBUG ) console.debug('>> editRightSidebar:', editRightSidebar )
			if ( editRightSidebar ) editRightSidebar.remove() // on Edit page only  // Why is [container|content].maxWidth = 'none' not respected when removed?

			const editTitle = document.querySelector('.js-post-editor-title-container')
			if ( DEBUG ) console.debug('>> editTitle:', editTitle )
			if ( editTitle ) editTitle.style.gridColumn = '1 /-1' // on Edit page only

			const editor = document.querySelector('[id^=post-editor]')
			if ( DEBUG ) console.debug('>> editor:', editor )
			editor.style.display = 'grid'
			editor.style.gridTemplateColumns = '1fr 8px 1fr'

			//const text = document.getElementsByName('textarea') // Why empty NodeList?!?
			const text = document.querySelector('[id^=wmd-input]')
			if ( DEBUG ) console.debug('>> text:', text )
			text.style.height = `${VIEWPORT_HEIGHT}px`

			const body = document.querySelector('[for^=wmd-input]')
			if ( DEBUG ) console.debug('>> body:', body )
			body.scrollIntoView()

			const formatHelp = document.querySelector('.pb12')
			if ( DEBUG ) console.debug('>> formatHelp:', formatHelp )
			if ( formatHelp ) formatHelp.remove() // on Edit page only

			const preview = document.querySelector('[id^=wmd-preview]')
			if ( DEBUG ) console.debug('>> preview:', preview )
			preview.style.border = 'solid 1px silver'
			preview.style.padding = '4px'

			console.log("END StackExchange - Tweak UI...delayed.")
		}, 1000 )

		console.log("END StackExchange - Tweak UI.")
	}
	catch (e) {
		console.error(e)
	}

})();
