// ==UserScript==
// @name        JANITOR – Java API Navigation Is The Only Rescue
// @description Inserts a navigation tree for modules, packages and types (interfaces, classes, enums, exceptions, errors, annotations) into the Javadoc pages of Java 11+.
// @version     23.06.10-08.08
// @author      Gerold 'Geri' Broser <https://stackoverflow.com/users/1744774>
// @icon        https://upload.wikimedia.org/wikipedia/commons/thumb/4/4d/Faenza-openjdk-6.svg/96px-Faenza-openjdk-6.svg.png
// @license     GNU GPLv3 <http://www.gnu.org/licenses/gpl-3.0.html>
// @homepage    https://gitlab.com/gerib/userscripts/-/wikis/JANITOR-%E2%80%93-Java-API-Navigation-Is-The-Only-Rescue
// @supportURL  https://gitlab.com/gerib/userscripts/-/issues
// @downloadURL https://gitlab.com/gerib/userscripts/-/raw/master/janitor/janitor.user.js
// @updateURL   https://gitlab.com/gerib/userscripts/-/raw/master/janitor/janitor.user.js
// @ ------------------------------------------------------------------------------------
// @namespace   igb
// @match       http*://docs.oracle.com/en/java/javase/*/docs/api/*
// @match       http://localhost/jdk*/docs/api/*
// @run-at      document-idle
// @grant       none
// ==/UserScript==

/**
 * Inspired by 'Missing iFrame view for Javadocs JDK 11+' at https://stackoverflow.com/q/51992347/1744774 .
 *
 * The original Javadoc page's DOM:
 *
 *   <body>
 *     <header>
 *     <main>
 *     <footer>
 *
 * is adapted to:
 *
 *   <body>
 *     <div id="JANITOR" style="display: flex;">
 *     | <div id="janitor.title" style="position: fixed; width: ${NAV_WIDTH};">
 *     |   <a>{title}
 *     |   <a>{show}
 *     | <div id="janitor.navigation" style="position: fixed; width: ${NAV_WIDTH};">
 *     |   <details id="janitor.mod.pkg.details">*¹ | <div id="janitor.type.details">*²
 *     |     <summary id="janitor.mod.pkg.summary">*¹ | <span id="janitor.type.summary">*²
 *     |       <span>{branch}
 *     |         <span>{icon}
 *     |           <a href='{module, package or type page}'>{module, package or type name}</a>
 *     <header style="margin-left: ${NAV_WIDTH}">
 *     <main style="margin-left: ${NAV_WIDTH}">
 *     <footer style="margin-left: ${NAV_WIDTH}">
 *
 *  ¹ for modules and packages
 *  ² for types and modules in java.se
 *
 * @see 'How to place div side by side' <https://stackoverflow.com/a/24292602/1744774>
 * @see 'How to create a collapsing tree table in html/css/js?' <https://stackoverflow.com/a/36222693/1744774>
 * @see '<div> with absolute position in the viewport when scrolling the page vertically' <https://stackoverflow.com/q/59417589/1744774>
 *
 * TODO
 *   - Replace synchronous (type)XHR with fetch() since it is deprecated.
 *   - Store/restore state of Show/Hide button.
 *   - Establish grouping by kind of types (instead of alphabetic order) in the tree view for Java 17+ as it was with Java 16- before.
 *     (Since eager loading of all types of a package has to be done anyway due to changes in the DOM with Java 17.)
 *   - Add content of modules that don't contain packages but just types, e.g. jdk.crypto.ec.
 *   - If a package has the same name as its enclosing module (e.g. Ⓜ java.sql > Ⓟ java.sql):
 *     - the module (and package) is not expanded if a type of the package is selected.
 *     - the package is wrongly expanded if a type of a sibling package (e.g. Ⓟ javax.sql) is selected.
 *   - If the selected package is a sub-package (e.g. java.lang.*) also the super-package (e.g. java.lang) is expanded (and highlighted) due the same string their names begin with.
 *   - Test with other browsers than Firefox v104 and Chrome v79.
 *   - Test with other userscript add-ons than Tampermonkey v4.17.
 */
'use strict'

// ----------------------------------------------------------------------------------------
// Customize to your liking
const TYPE_LETTERS_IN_CIRCLE = false // Changed default to »false« since on openSUSE Tumbleweed:
// - with FF 104.0.2 the circled letters are rendered ludicrously large
// - with Opera 91.0.4516.16 and Chromium 105.0.5195.127 the circled
//   letters are rendered ludicrously small
// - the font height of the circled letters is way to large compared to the latin letters
const COLORS = new Map(
	[['Module', "black"], ['Package', "purple"], ['Interface', "dodgerblue"], ['Class', "blue"],
	['Enum', "green"], ['Exception', "orange"], ['Error', "red"], ['Annotation', "brown"]])
// ----------------------------------------------------------------------------------------

const NAV_WIDTH = '30em'
const NAV_WIDTH_HIDE = '2em'
const DEV = true // set to »true« while developing
const DEBUG = false // set to »true« for debugging
const API_URL = document.URL.substring(0, document.URL.indexOf("/api") + 4)
if (DEV) console.debug(`→ API_URL = ${API_URL}`)
const ICONS = new Map(TYPE_LETTERS_IN_CIRCLE
	? [['Module', "Ⓜ"], ['Package', "Ⓟ"], ['Interface', "Ⓘ"], ['Class', "Ⓒ"], ['Enum', "Ⓔ<sub>n</sub>"],
	['Exception', "Ⓔ<sub>x</sub>"], ['Error', "Ⓔ<sub>r</sub>"], ['Annotation', "Ⓐ"]]
	: [['Module', "M"], ['Package', "P"], ['Interface', "I"], ['Class', "C"], ['Enum', "E<sub>n</sub>"],
	['Exception', "E<sub>x</sub>"], ['Error', "E<sub>r</sub>"], ['Annotation', "A"]])
// See 'How to get the browser viewport dimensions?' at https://stackoverflow.com/a/8876069/1744774
const VIEWPORT_HEIGHT = Math.max(document.documentElement.clientHeight, window.innerHeight || 0) - 28
const VIEWPORT_HALF = VIEWPORT_HEIGHT >>> 1

JANITOR()

function JANITOR() {

	try {
		console.log("BEGIN JANITOR – Java API Navigation Is The Only Rescue...")

		// Create navigation tree
		const janitor = document.createElement('div')
		janitor.id = 'JANITOR'

		const title = document.createElement('div')
		title.id = 'janitor.title'
		title.style.position = 'fixed'
		title.style.width = NAV_WIDTH
		title.style.borderBottom = '1px solid'
		title.style.padding = '3px'
		title.style.textAlign = 'center'

		const a = document.createElement('a')
		a.href = 'https://github.com/gerib/userscripts/wiki/JANITOR-%E2%80%93-Java-API-Navigation-Is-The-Only-Rescue'
		a.target = "_blank"
		a.innerText = "JANITOR – Java API Navigation Is The Only Rescue"
		a.title = "JANITOR – Java API Navigation Is The Only Rescue"

		const navigation = document.createElement('div')
		navigation.id = 'janitor.navigation'
		navigation.style.width = NAV_WIDTH
		navigation.style.height = `${VIEWPORT_HEIGHT}px`
		navigation.style.top = '24px'
		navigation.style.position = 'fixed'
		navigation.style.paddingLeft = '4px'
		//navigation.style.borderRight = '1px solid'
		navigation.style.overflowY = 'scroll'
		navigation.style.paddingTop = '3px'

		// Rearrange existing elements
		const header = document.getElementsByTagName('header')[0]
		header.style.marginLeft = NAV_WIDTH
		const h1 = document.querySelector('body > div.header') // for Java 11 Overview page
		if (h1)
			h1.style.marginLeft = NAV_WIDTH
		let nav = document.querySelector('div.fixedNav') // for Java <=13
		if (nav)
			document.querySelector('div.fixedNav').style.width = 'auto'
		const main = document.getElementsByTagName('main')[0]
		main.style.marginLeft = NAV_WIDTH
		const footer = document.getElementsByTagName('footer')[0]
		footer.style.marginLeft = NAV_WIDTH

		// Add navigation to DOM
		title.appendChild(a)
		title.appendChild(showAndHideButton(navigation, header, h1, title, a, main, footer))
		janitor.appendChild(title)
		janitor.appendChild(navigation)
		header.parentNode.insertBefore(janitor, header)
		addTreeNodes('Module', navigation, API_URL, navigation, '', '', '', '')

		console.log("END JANITOR – Java API Navigation Is The Only Rescue.")
	}
	catch (e) {
		console.error(e)
	}

} // JANITOR()

function addTreeNodes(ofType, navigation, fromURL, toParent, parentName, moduleName, packageName, classTypesCount) {

	const isJava15 = fromURL.includes("/15/")
	const isJava15plus = /\/(1[5-9]|[2-9]\d)\//.test(fromURL);
	const isJava16plus = /\/(1[6-9]|[2-9]\d)\//.test(fromURL);
	const isJava17plus = /\/(1[7-9]|[2-9]\d)\//.test(fromURL);

	if (DEV) console.debug("→ Adding",
		ofType + "(s)",
		// \u200B ... zero-width space
		moduleName === ''
				? "\u200B"
				: `for ${moduleName}/${packageName}`,
		"of", parentName === ''
				? "API"
				: `module ${parentName}`,
		fromURL === API_URL ?
				"\u200B"
				: `from ${fromURL.substring(fromURL.indexOf('javase') + 18)}`,
		"to", toParent.id)

	const types = "'Module', 'Package', 'Interface', 'Class', 'Enum', 'Exception', 'Error', 'Annotation'"
	if (types.search(ofType) < 0)
		throw `function addTreeNodes(): Illegal argument ofType='${ofType}'. Only ${types} allowed.`

	const containerXHR = new XMLHttpRequest()
	containerXHR.responseType = "document"
	containerXHR.addEventListener('load', function(event) {
		if (DEBUG) console.debug("→ event:", event)
		if (DEV) console.debug("→ statusText:", containerXHR.statusText, "→ response:", containerXHR.response)

		let containerDoc = containerXHR.response

		// CSS selector for links <ofType> on page denoted by <fromURL>
		let selector

		const containers = "'Module', 'Package'"
		if (containers.search(ofType) > 0) { // #### add containers ####

			if (ofType === 'Module') {
				selector = '.overviewSummary th > a' // Java 11: table.overviewSummary, Java 12-14: div.overviewSummary
				if (isJava15)
					selector = 'table.summary-table th > a'
				else if (isJava16plus)
					selector = 'div.summary-table div > a'
			}
			else if (ofType === 'Package' || parentName === "java.se") {
				selector = '.packagesSummary th > a' // Java 11: table.packagesSummary, Java 12-14: div.packagesSummary
				if (isJava15)
					selector = parentName === "java.se"
							? 'table.details-table th > a'
							: 'table.summary-table th > a'
				if (isJava16plus)
					selector = parentName === "java.se"
							? selector = 'div.details-table div > a'
							: selector = 'div.summary-table div > a'
			}

			const links = containerDoc.querySelectorAll(`${selector}`)
			let nodeCount = links.length
			if (DEV) console.debug("→ selector: ", selector)
			if (DEV) console.debug("  →", nodeCount, `link(s) for ${ofType}(s) in`,	parentName === ''	? "API"	: "module " + parentName, links)

			for (let link of links) {

				let branch = `<span style='color:${COLORS.get(ofType)};'>${ICONS.get(ofType)}</span>`
				if (ofType === 'Package' || parentName === "java.se")
					branch = `${parentName === "java.se"
							? "&nbsp; &nbsp;"
							: ""}${--nodeCount > 0
									? "├"
									: "└"}─ ${branch}`

				const details = parentName === "java.se"
						? document.createElement('div')
						: document.createElement('details')
				details.id = 'janitor.mod.pkg.details'
				const summary = parentName === "java.se"
						? document.createElement('span')
						: document.createElement('summary')
				summary.id = 'janitor.mod.pkg.summary'
				const a = link
				//a.href = `${API_URL}/${ parentName === "java.se"
				//		? ""
				//		: parentName + "/"}${a.getAttribute('href')}`
				a.href = `${API_URL}/${parentName + "/"}${a.getAttribute('href')}`

				a.title = `${ofType} ${a.innerText}`
				summary.innerHTML = `<span title="${a.title}" style="cursor:default;">${branch}&nbsp;&nbsp;</span>` //  ...type kinds loading...

				if (parentName !== "java.se")
					summary.addEventListener('click', function() {
						ofType === 'Module'
							? a.innerText === 'java.se'
								? addTreeNodes('Module', navigation, a.href, details, a.innerText, '', '', 0)
								: addTreeNodes('Package', navigation, a.href, details, a.innerText, '', '', 0)
							: addTreeNodes('Interface', navigation, a.href, details, '', parentName, a.innerText, -1)
					}, { once: true })

				summary.appendChild(a)
				details.appendChild(summary)
				toParent.appendChild(details)

				// expand and highlight navigation tree of current module or package page
				// didn't find out yet why the following is needed, but otherwise tree expansion doesn't work on type pages
				let postfix = isJava15plus
						? ""
						: "package"
				if (document.URL.includes(`${a.innerText}/`) || // module
						document.URL.includes(`${a.innerText.replace(/\./g, "/")}/${postfix}`) ) {
					summary.style.fontWeight = 'bold'
					summary.click()
					navigation.scrollTo(0, summary.offsetTop - navigation.clientHeight / 2)
				}

				const span = document.querySelector('span.packageLabelInType')
				if (span && span.parentNode.lastChild.innerHTML === a.innerText) {
					summary.click()
				}

			} // for ( links )

		} // add containers
		else { // #### add types ####

			if (classTypesCount < 0) {
				classTypesCount = containerDoc.querySelectorAll('.typeSummary th > a').length // Java 11-14
				if (isJava15)
					classTypesCount = containerDoc.querySelectorAll('.summary-table th > a').length
				else if (isJava16plus)
					classTypesCount = containerDoc.querySelectorAll('ul div.col-first > a').length
			}

			// Used to select different type section headers (Interface, Class, Enum, Exception, Error, Annotation) below
			// since there's still no CSS selector for <innerText>.
			let selector = isJava16plus
					? 'div.table-header.col-first'
					: 'caption > span'
			for (const type of containerDoc.querySelectorAll(selector))
				type.setAttribute('type', type.innerText)

			if (isJava16plus)
				selector = `div.table-header.col-first[type^="${ofType}"]`
			else
				selector = `caption > span[type^="${ofType}"]`

			const typeSectionHeader = containerDoc.querySelector(selector)
			let links = null
			if (typeSectionHeader) {
				if (isJava16plus)
					links = typeSectionHeader.parentNode.querySelectorAll(`div.col-first > a`)
					//            div.caption<----li----+
				else // Java 11-15
					links = typeSectionHeader.parentNode.parentNode.querySelectorAll('th > a')
					//                   span<-caption--<--table--+

				if (DEV) console.debug("→ selector: ", selector)
				if (DEV) console.debug("  →", links.length, "link(s) for", ofType + "(s) in", moduleName + "/" + packageName, links)

				// "Security Error: Content at https://docs.oracle.com/en/java/javase/18/docs/api/java.base/java/io/package-summary.html
				// may not load data from https://gitlab.com/gerib/userscripts/-/raw/master/janitor/janitor-navigation-loader.js."
				//let typeKindCollector = new Worker('https://gitlab.com/gerib/userscripts/-/raw/master/janitor/janitor-navigation-loader.js')

				const types = new Map();
				if (isJava17plus) {

					for (const link of links) {

						// TODO replace with fetch() since synchronous XHR is deprecated
						const typeXHR = new XMLHttpRequest()
						//typeXHR.responseType = "document" // synchronous XMLHttpRequests do not support timeout and responseType ...
						typeXHR.addEventListener('load', function(event) {
							if (DEBUG) console.debug("→ event:", event)
							if (DEBUG) console.debug("→ statusText:", typeXHR.statusText, "→ response:", typeXHR.response)

							// ... therefore:
							//let doc = typeXHR.response
							const doc = document.implementation.createHTMLDocument('http://www.w3.org/1999/xhtml', 'html');
							doc.open()
							doc.write(typeXHR.response)
							doc.close()

							types.set(link.innerText, doc.querySelector('h1').title)
						}) // page load listener
						typeXHR.open('GET', link.href, false)
						typeXHR.send()
					} // for ( links )
				}
				if (DEBUG) types.forEach((value, key) => { console.debug(`${key} -> ${value.split(' ')[0]}`) })

				let previousTypeName
				for (const link of links) {

					if (link.innerText === previousTypeName) {
						classTypesCount--
						continue
					}

					const details = document.createElement('div')
					details.id = 'janitor.type.details'
					const summary = document.createElement('span')
					summary.id = 'janitor.type.summary'
					const a = link
					a.href = `${API_URL}/${moduleName}/${packageName.replace(/\./g, "/")}/${a.getAttribute('href')}`
					a.title = `${ofType} ${a.innerText}`
					const doHighlight = document.URL.includes(`/${a.innerText}.html`)

					let typeColor, typeIcon
					if (isJava17plus) {

						let typeTitle = types.get(a.innerText)
						let typeKind = typeTitle.split(' ')[0]

						typeColor = COLORS.get(typeKind)
						typeIcon = ICONS.get(typeKind)
					}
					else {  // Java 11-16
						typeColor = COLORS.get(ofType)
						typeIcon = ICONS.get(ofType)
					}
					let icon = `<span style='color:${typeColor};${doHighlight ? 'font-weight:bold': ''}'>${typeIcon}</span>`
					const branch = `&nbsp; &nbsp; ·&nbsp; &nbsp;&thinsp;${--classTypesCount > 0
							? "├"
							: "└"}─ ${icon}`
					summary.innerHTML = `<span title='${a.title}' style='cursor:default;'>${branch}&nbsp;&nbsp;</span>`

					summary.appendChild(a)
					details.appendChild(summary)
					toParent.appendChild(details)

					// highlight tree item of current type page
					if (doHighlight) {
console.debug('####', details.parentNode)
						details.parentNode.firstChild.style.fontWeight = 'bold'
						a.style.fontWeight = 'bold'
						navigation.scrollTo(0, summary.offsetTop - navigation.clientHeight / 2)
					}
					previousTypeName = a.innerText

				} // for ( links )
			} // if ( section <ofType> exists )

			if (ofType === 'Interface')
				addTreeNodes('Class', navigation, fromURL, toParent, '', moduleName, packageName, classTypesCount)
			else if (ofType === 'Class')
				addTreeNodes('Enum', navigation, fromURL, toParent, '', moduleName, packageName, classTypesCount)
			else if (ofType === 'Enum')
				addTreeNodes('Exception', navigation, fromURL, toParent, '', moduleName, packageName, classTypesCount)
			else if (ofType === 'Exception')
				addTreeNodes('Error', navigation, fromURL, toParent, '', moduleName, packageName, classTypesCount)
			else if (ofType === 'Error')
				addTreeNodes('Annotation', navigation, fromURL, toParent, '', moduleName, packageName, classTypesCount)

		} // add types

	}) // page load listener
	containerXHR.open('GET', fromURL)
	containerXHR.send()

} // addTreeNodes()

function showAndHideButton(navigation, header, h1, title, a, main, footer) {

		const show = document.createElement('a')
		show.href = '#'
		show.innerText = "<< "
		show.title = "Hide JANITOR"
		show.style.paddingRight = '8px'
		show.style.float = 'right'
		show.onclick = function() {
			if (navigation.style.width == NAV_WIDTH) {
				show.innerText = ">>"
				show.title = "Show JANITOR"
				show.style.paddingRight = '5px'
				a.style.display = 'none'
				navigation.style.display = 'none'

				title.style.width = NAV_WIDTH_HIDE
				navigation.style.width = NAV_WIDTH_HIDE
				header.style.marginLeft = NAV_WIDTH_HIDE
				if (h1)
					h1.style.marginLeft = NAV_WIDTH_HIDE
				main.style.marginLeft = NAV_WIDTH_HIDE
				footer.style.marginLeft = NAV_WIDTH_HIDE
			}
			else {
				show.innerText = "<< "
				show.title = "Hide JANITOR"
				show.style.paddingRight = '8px'
				a.style.display = 'inline'
				navigation.style.display = 'block'

				title.style.width = NAV_WIDTH
				navigation.style.width = NAV_WIDTH
				header.style.marginLeft = NAV_WIDTH
				if (h1)
					h1.style.marginLeft = NAV_WIDTH
				main.style.marginLeft = NAV_WIDTH
				footer.style.marginLeft = NAV_WIDTH
			}
			return false;
		}
	return show

} // showAndHideButton()
