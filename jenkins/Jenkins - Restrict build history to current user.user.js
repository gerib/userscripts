// ==UserScript==
// @name        Jenkins - Restrict build history to current user
// @description Removes builds from Build History that are not started by the currently logged on user
// @version     21.10.29-1240
// @author      Gerold 'Geri' Broser <https://stackoverflow.com/users/1744774>
// @icon        https://aws1.discourse-cdn.com/business20/uploads/jenkins/original/1X/bad74f3fd144ac08a4b230bd0782bfc0ea77a683.png
// @license     GNU GPLv3 <http://www.gnu.org/licenses/gpl-3.0.html>
// @homepage    https://gitlab.com/gerib/userscripts/-/wikis/Jenkins - Restrict build history to current user
// @supportURL  https://gitlab.com/gerib/userscripts/-/issues
// @downloadURL https://gitlab.com/gerib/userscripts/-/raw/master/jenkins/Jenkins%20-%20Restrict%20build%20history%20to%20current%20user.user.js
// @updateURL   https://gitlab.com/gerib/userscripts/-/raw/master/jenkins/Jenkins%20-%20Restrict%20build%20history%20to%20current%20user.user.js
// --------------------------------------------------
// @namespace   igb
//              --- Adapt according to your JENKINS_URL ---
// @include     /http://xmg:8080/job/SO-69728877.*/
// @run-at      document-idle
// @grant       none
// ==/UserScript==

(function() {
  'use strict';

	// Has to be the same as defined in the Build Name and Description Setter plugin (https://plugins.jenkins.io/build-name-setter/)
	const USERNAME_PREFIX = 'Started by'

	const DEV = true // set to »true« while developing
	const DEBUG = false // set to »true« for debugging

	try {
		console.log("BEGIN Jenkins – Restrict build history to current user...")

		let buildHistoryTitle = document.querySelector('#buildHistory span[class*="pane-header-title"]')
		if (DEV) console.debug( "buildHistoryTitle.childNodes:", buildHistoryTitle.childNodes )

		const span = document.createElement('span')
		span.innerHTML = "<br />Filtered by userscript! Disable it to see all builds."
		span.style.color = "red"
		span.style.fontSize = "smaller"
		span.style.fontWeight = "normal"
		buildHistoryTitle.appendChild(span)

		const currentUser = document.querySelector('#header span[class*="hidden-xs"]').innerText
		if (DEV) console.debug( 'currentUser:', currentUser )

		const buildRows = document.querySelectorAll('#buildHistory tr[class*="build-row"]')
		if (DEV) console.debug( buildRows )

		buildRows.forEach( buildRow => {
			let buildRowA = buildRow.querySelector('a[class*=display-name]')
			let rowText = ''
			if ( buildRowA /*not null*/ )
				rowText = buildRowA.innerText
			let keepText = `${USERNAME_PREFIX} ${currentUser}`
			if (DEV && buildRowA /*not null*/ ) console.debug( "buildRow.parent:", buildRow.parentNode, "\nbuildRow:", buildRow, "\nrowText:", rowText, "\nkeepText:", keepText )
			if ( buildRow.parentNode /*not null*/ && rowText.indexOf(keepText) == -1 ) {
				console.info(`Removing ${buildRow}...`)
				buildRow.remove()
			}
		})

		console.log("END Jenkins – Restrict build history to current user.")
	}
	catch (e) {
		console.error(e)
	}

})();
